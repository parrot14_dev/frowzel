use std::mem::replace;

use common::{get_icon_path, Browser, IconPath};
use iced::{
    executor,
    widget::{
        button, column, container, horizontal_space, row, scrollable, text, text_input,
        vertical_space, Column, Image, Row, Svg,
    },
    window::{self, Id},
    Application, Command, Length, Settings, Size, Theme,
};
use util::get_browsers;

fn main() -> iced::Result {
    FrowzelConfig::run(Settings {
        window: window::Settings {
            size: Size {
                width: 550f32,
                height: 250f32,
            },
            resizable: false,
            ..window::Settings::default()
        },
        default_text_size: 16.into(),
        ..Settings::default()
    })
}

enum View {
    Main,
    Add(Vec<(Browser, Option<IconPath>)>),
    Config {
        index: Option<usize>,
        browser: (Browser, Option<IconPath>),
        icon_input: String,
    },
    Confirm,
}

struct FrowzelConfig {
    browsers: Vec<(Browser, Option<IconPath>)>,
    view: View,
    unsaved: bool,
}

#[derive(Debug, Clone)]
pub enum MainMessage {
    ChangeAdd,
    ChangeConfig(usize),
    Exit,
    Save,
}

#[derive(Debug, Clone)]
pub enum AddMessage {
    ChangeMain,
    ChangeConfig((Browser, Option<IconPath>)),
}

#[derive(Debug, Clone)]
pub enum EditMessage {
    Save,
    Cancel,
    UpdateIconInput(String),
    UpdateIcon,
    UpdateNameInput(String),
    UpdateExecInput(String),
}

#[derive(Debug, Clone)]
pub enum ConfirmMessage {
    Exit,
    SaveExit,
}

#[derive(Debug, Clone)]
pub enum Message {
    MainMessage(MainMessage),
    AddMessage(AddMessage),
    EditMessage(EditMessage),
    ConfirmMessage(ConfirmMessage),
}

impl Application for FrowzelConfig {
    type Executor = executor::Default;

    type Message = Message;

    type Theme = Theme;

    type Flags = ();

    fn new(_flags: Self::Flags) -> (Self, iced::Command<Self::Message>) {
        (
            Self {
                browsers: common::read_config(),
                view: View::Main,
                unsaved: false,
            },
            Command::none(),
        )
    }

    fn title(&self) -> String {
        String::from("Frowzel Config")
    }

    fn update(&mut self, message: Self::Message) -> iced::Command<Self::Message> {
        match message {
            Message::MainMessage(msg) => match msg {
                MainMessage::ChangeAdd => self.view = View::Add(get_browsers()),
                MainMessage::ChangeConfig(index) => {
                    let browser = self.browsers.get(index).unwrap().clone();
                    let icon_input = browser.0.icon.clone();
                    self.view = View::Config {
                        index: Some(index),
                        browser,
                        icon_input,
                    };
                }
                MainMessage::Exit => {
                    if self.unsaved {
                        self.view = View::Confirm;
                    } else {
                        return window::close(Id::MAIN);
                    }
                }
                MainMessage::Save => {
                    if Self::save(&self.browsers) {
                        self.unsaved = false;
                    }
                }
            },
            Message::AddMessage(msg) => match msg {
                AddMessage::ChangeMain => self.view = View::Main,
                AddMessage::ChangeConfig(mut browser) => {
                    browser.1 = get_icon_path(&browser.0.icon, 128);
                    let icon_input = browser.0.icon.clone();
                    self.view = View::Config {
                        index: None,
                        browser,
                        icon_input,
                    };
                }
            },
            Message::EditMessage(msg) => match msg {
                EditMessage::Save => {
                    if let View::Config { browser, index, .. } = replace(&mut self.view, View::Main)
                    {
                        self.unsaved = true;
                        match index {
                            Some(index) => {
                                let entry = self.browsers.get_mut(index).unwrap();
                                *entry = browser;
                            }
                            None => self.browsers.push(browser),
                        }
                    }
                }
                EditMessage::Cancel => self.view = View::Main,
                EditMessage::UpdateIconInput(new_icon) => {
                    if let View::Config { icon_input, .. } = &mut self.view {
                        *icon_input = new_icon;
                    }
                }
                EditMessage::UpdateIcon => {
                    if let View::Config {
                        icon_input,
                        browser,
                        ..
                    } = &mut self.view
                    {
                        browser.0.icon = icon_input.clone();
                        browser.1 = get_icon_path(icon_input, 128);
                    }
                }
                EditMessage::UpdateNameInput(new_name) => {
                    if let View::Config {
                        browser: (Browser { name, .. }, ..),
                        ..
                    } = &mut self.view
                    {
                        *name = new_name;
                    }
                }
                EditMessage::UpdateExecInput(new_exec) => {
                    if let View::Config {
                        browser: (Browser { exec, .. }, ..),
                        ..
                    } = &mut self.view
                    {
                        *exec = new_exec;
                    }
                }
            },
            Message::ConfirmMessage(msg) => match msg {
                ConfirmMessage::Exit => return window::close(Id::MAIN),
                ConfirmMessage::SaveExit => {
                    if Self::save(&self.browsers) {
                        self.unsaved = false;
                    }
                    return window::close(Id::MAIN);
                }
            },
        }
        Command::none()
    }

    fn view(&self) -> iced::Element<'_, Self::Message, Self::Theme> {
        let view = match &self.view {
            View::Main => Self::main_view(&self.browsers, &self.unsaved).map(Message::MainMessage),
            View::Add(browsers) => Self::add_view(browsers).map(Message::AddMessage),
            View::Config {
                browser,
                icon_input,
                ..
            } => Self::config_view(browser, icon_input).map(Message::EditMessage),
            View::Confirm => Self::confirm_view().map(Message::ConfirmMessage),
        };
        container(container(view).height(230).center_y())
            .center_x()
            .center_y()
            .height(Length::Fill)
            .width(Length::Fill)
            .into()
    }
}

impl FrowzelConfig {
    fn main_view<'a>(
        browsers: &'a [(Browser, Option<IconPath>)],
        unsaved: &bool,
    ) -> iced::Element<'a, MainMessage, Theme> {
        row![container(
            scrollable(
                browsers
                    .iter()
                    .enumerate()
                    .fold(Row::new(), |row, (index, browser)| {
                        row.push(
                            button(
                                container(
                                    column![
                                        if let Some(path) = &browser.1 {
                                            match path {
                                                common::IconPath::SVG(path) => Svg::from_path(path)
                                                    .width(128)
                                                    .height(128)
                                                    .into(),
                                                common::IconPath::Image(path) => {
                                                    Image::new(path).width(128).height(128).into()
                                                }
                                            }
                                        } else {
                                            Into::<iced::Element<_>>::into(
                                                vertical_space().height(128),
                                            )
                                        },
                                        {
                                            let Browser { name, .. } = &browser.0;
                                            text(name).size(20)
                                        }
                                    ]
                                    .align_items(iced::Alignment::Center),
                                )
                                .width(150)
                                .height(Length::Fill)
                                .center_x()
                                .center_y(),
                            )
                            .on_press(MainMessage::ChangeConfig(index))
                            .padding(10),
                        )
                    })
                    .spacing(10)
                    .height(230)
            )
            .direction(scrollable::Direction::Horizontal(
                scrollable::Properties::default()
            ))
        )
        .max_width(350)]
        .push(
            column![
                button(
                    text("Nuevo")
                        .height(150)
                        .width(150)
                        .horizontal_alignment(iced::alignment::Horizontal::Center)
                        .vertical_alignment(iced::alignment::Vertical::Center),
                )
                .on_press(MainMessage::ChangeAdd)
                .padding(10),
                row![
                    button(
                        text("Guardar")
                            .height(Length::Fill)
                            .width(60)
                            .horizontal_alignment(iced::alignment::Horizontal::Center)
                            .vertical_alignment(iced::alignment::Vertical::Center),
                    )
                    .on_press_maybe(if *unsaved {
                        Some(MainMessage::Save)
                    } else {
                        None
                    })
                    .padding(10),
                    button(
                        text("Salir")
                            .height(Length::Fill)
                            .width(60)
                            .horizontal_alignment(iced::alignment::Horizontal::Center)
                            .vertical_alignment(iced::alignment::Vertical::Center),
                    )
                    .on_press(MainMessage::Exit)
                    .padding(10),
                ]
                .spacing(10)
            ]
            .spacing(10),
        )
        .spacing(10)
        .height(230)
        .into()
    }

    fn add_view(browsers: &[(Browser, Option<IconPath>)]) -> iced::Element<'_, AddMessage, Theme> {
        let browsers = browsers
            .iter()
            .map(|browser| {
                let browser_row = row![
                    if let Some(path) = &browser.1 {
                        match path {
                            common::IconPath::SVG(path) => {
                                Svg::from_path(path).width(40).height(40).into()
                            }
                            common::IconPath::Image(path) => {
                                Image::new(path).width(40).height(40).into()
                            }
                        }
                    } else {
                        Into::<iced::Element<_>>::into(horizontal_space().width(40))
                    },
                    text(browser.0.name.clone())
                ]
                .spacing(10)
                .height(40)
                .width(Length::Fill)
                .align_items(iced::Alignment::Center);

                button(browser_row).on_press(AddMessage::ChangeConfig(browser.clone()))
            })
            .fold(Column::new(), |col, but| col.push(but));

        column![
            text("Desde navegador"),
            scrollable(
                browsers.push(
                    button(
                        row![horizontal_space().width(40), text("Generico")]
                            .spacing(10)
                            .height(40)
                            .width(Length::Fill)
                            .align_items(iced::Alignment::Center)
                    )
                    .on_press(AddMessage::ChangeConfig((Browser::default(), None)))
                )
            )
            .height(150),
            button("Volver").on_press(AddMessage::ChangeMain)
        ]
        .width(300)
        .align_items(iced::Alignment::Center)
        .spacing(10)
        .into()
    }

    fn config_view<'a>(
        browser: &'a (Browser, Option<IconPath>),
        icon_input: &str,
    ) -> iced::Element<'a, EditMessage, Theme> {
        let Browser { exec, name, .. } = &browser.0;
        column![
            row![
                if let Some(path) = &browser.1 {
                    match path {
                        common::IconPath::SVG(path) => {
                            Svg::from_path(path).width(128).height(128).into()
                        }
                        common::IconPath::Image(path) => {
                            Image::new(path).width(128).height(128).into()
                        }
                    }
                } else {
                    Into::<iced::Element<_>>::into(horizontal_space().height(128))
                },
                column![
                    text("Icono :")
                        .height(30)
                        .vertical_alignment(iced::alignment::Vertical::Center),
                    text("Nombre :")
                        .height(30)
                        .vertical_alignment(iced::alignment::Vertical::Center),
                    text("Orden :")
                        .height(30)
                        .vertical_alignment(iced::alignment::Vertical::Center),
                ]
                .spacing(10)
                .align_items(iced::Alignment::End),
                column![
                    text_input("xdg-icon", icon_input)
                        .on_input(EditMessage::UpdateIconInput)
                        .on_submit(EditMessage::UpdateIcon),
                    text_input("Navegador", name).on_input(EditMessage::UpdateNameInput),
                    text_input("/path/to/bin %u", exec).on_input(EditMessage::UpdateExecInput),
                ]
                .spacing(10)
            ]
            .align_items(iced::Alignment::Center)
            .spacing(10)
            .width(500),
            row![
                button("Cancelar").on_press(EditMessage::Cancel),
                button("Guardar").on_press(EditMessage::Save)
            ]
        ]
        .spacing(20)
        .align_items(iced::Alignment::Center)
        .into()
    }

    fn confirm_view() -> iced::Element<'static, ConfirmMessage, Theme> {
        row![
            button("Guardar y Salir").on_press(ConfirmMessage::SaveExit),
            button("Salir").on_press(ConfirmMessage::Exit)
        ]
        .spacing(10)
        .into()
    }

    fn save(browsers: &[(Browser, Option<IconPath>)]) -> bool {
        let browsers = common::BrowsersConfig {
            browsers: browsers.into_iter().map(|val| val.0.clone()).collect(),
        };

        if let Some(json) = browsers.to_json() {
            return common::write_config(json);
        }
        false
    }
}

mod util {

    use common::{get_icon_path, Browser, IconPath};
    use freedesktop_desktop_entry::{default_paths, DesktopEntry, Iter};
    use std::{borrow::Cow, fs};

    pub fn get_browsers() -> Vec<(Browser, Option<IconPath>)> {
        let mut browsers: Vec<(Browser, Option<IconPath>)> = Vec::new();

        for path in Iter::new(default_paths()) {
            if let Ok(bytes) = fs::read_to_string(&path) {
                if let Ok(entry) = DesktopEntry::decode(&path, &bytes) {
                    if let Some(mime_type) = entry.mime_type() {
                        if let (true, Some(exec)) =
                            (mime_type.contains("x-scheme-handler/http"), entry.exec())
                        {
                            let icon = entry.icon().unwrap_or("internet-web-browser");
                            browsers.push((
                                Browser {
                                    icon: icon.into(),
                                    name: entry
                                        .name(None)
                                        .unwrap_or(Cow::Borrowed("Navegador"))
                                        .into(),
                                    exec: exec.into(),
                                },
                                get_icon_path(icon, 48),
                            ))
                        }
                    }
                }
            }
        }

        return browsers;
    }
}
