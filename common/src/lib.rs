use std::{
    fs::{read_to_string, File},
    io::Write,
    path::PathBuf,
};

use linicon::IconType;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone)]
pub enum IconPath {
    SVG(PathBuf),
    Image(PathBuf),
}

pub fn get_icon_path(icon: &str, size: u16) -> Option<IconPath> {
    linicon::lookup_icon(icon)
        .filter_map(Result::ok)
        .filter(|icon| icon.max_size >= size)
        .next()
        .map(|v| {
            if let IconType::SVG = v.icon_type {
                IconPath::SVG(v.path)
            } else {
                IconPath::Image(v.path)
            }
        })
}

pub fn get_config_dir() -> Option<PathBuf> {
    dirs::config_local_dir().map(|p| p.join("frowzel"))
}

pub fn write_config(json: String) -> bool {
    if let None = get_config_dir()
        .and_then(|path| File::create(path).ok())
        .and_then(|mut file| file.write_all(json.as_bytes()).ok())
    {
        return false;
    }
    true
}

pub fn read_config() -> Vec<(Browser, Option<IconPath>)> {
    if let Some(BrowsersConfig { browsers }) = get_config_dir()
        .filter(|path| path.exists())
        .and_then(|path| read_to_string(path).ok())
        .and_then(|json| serde_json::from_str::<BrowsersConfig>(&json).ok())
    {
        return browsers
            .into_iter()
            .map(|browser| {
                let icon_path = get_icon_path(&browser.icon, 128);
                (browser, icon_path)
            })
            .collect();
    } else {
        vec![]
    }
}

#[derive(Deserialize, Serialize)]
pub struct BrowsersConfig {
    pub browsers: Vec<Browser>,
}

impl BrowsersConfig {
    pub fn to_json(&self) -> Option<String> {
        serde_json::to_string(self).ok()
    }

    pub fn from_json(json: String) -> Option<Self> {
        serde_json::from_str(&json).ok()
    }
}

#[derive(Deserialize, Serialize, Clone, Debug, Default)]
pub struct Browser {
    pub icon: String,
    pub name: String,
    pub exec: String,
}
