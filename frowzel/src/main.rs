use std::{env, process::{self, ExitCode}};

use common::{Browser, IconPath};
use iced::{
    executor,
    widget::{
        button, column, container, row, scrollable, text, text_input, vertical_space, Image, Row,
        Svg,
    },
    window::{self, Id},
    Application, Command, Length, Settings, Size, Theme,
};

fn main() -> ExitCode {
    Frowzel::run(Settings {
        window: window::Settings {
            decorations: false,
            size: Size {
                width: 550f32,
                height: 285f32,
            },
            resizable: false,
            ..window::Settings::default()
        },
        default_text_size: 16.into(),
        flags: (
            common::read_config(),
            env::args().skip(1).next().expect("No URI Provided"),
        ),
        ..Settings::default()
    })
    .map(|_| ExitCode::SUCCESS)
    .unwrap_or(ExitCode::FAILURE)
}

#[derive(Debug, Clone)]
pub enum Message {
    Open(usize),
    Copy,
    Exit,
}

struct Frowzel {
    browsers: Vec<(Browser, Option<IconPath>)>,
    link: String,
}

impl Application for Frowzel {
    type Executor = executor::Default;

    type Message = Message;

    type Theme = Theme;

    type Flags = (Vec<(Browser, Option<IconPath>)>, String);

    fn new(flags: Self::Flags) -> (Self, iced::Command<Self::Message>) {
        (
            Self {
                browsers: flags.0,
                link: flags.1,
            },
            Command::none(),
        )
    }

    fn title(&self) -> String {
        String::from("Metadalp")
    }

    fn update(&mut self, message: Self::Message) -> iced::Command<Self::Message> {
        match message {
            Message::Open(index) => {
                let exec = &self
                    .browsers
                    .get(index)
                    .unwrap()
                    .0
                    .exec
                    .replace("%u", "\"$uri\"");

                process::Command::new("sh")
                    .arg("-c")
                    .arg(format!("exec {}", exec))
                    .env("uri", &self.link)
                    .spawn()
                    .expect("Failed to run browser");

                return window::close(Id::MAIN);
            }
            Message::Copy => todo!(),
            Message::Exit => window::close(Id::MAIN),
        }
    }

    fn view(&self) -> iced::Element<'_, Self::Message, Self::Theme> {
        container(
            column![
                row![
                    text_input("", &self.link).size(19),
                    button("Copiar").height(35),
                    button("Salir").height(35)
                ],
                container(
                    scrollable(
                        self.browsers
                            .iter()
                            .enumerate()
                            .fold(Row::new(), |row, (index, browser)| {
                                row.push(
                                    button(
                                        container(
                                            column![
                                                if let Some(path) = &browser.1 {
                                                    match path {
                                                        common::IconPath::SVG(path) => {
                                                            Svg::from_path(path)
                                                                .width(128)
                                                                .height(128)
                                                                .into()
                                                        }
                                                        common::IconPath::Image(path) => {
                                                            Image::new(path)
                                                                .width(128)
                                                                .height(128)
                                                                .into()
                                                        }
                                                    }
                                                } else {
                                                    Into::<iced::Element<_>>::into(
                                                        vertical_space().height(128),
                                                    )
                                                },
                                                {
                                                    let Browser { name, .. } = &browser.0;
                                                    text(name).size(20)
                                                }
                                            ]
                                            .align_items(iced::Alignment::Center),
                                        )
                                        .width(150)
                                        .height(Length::Fill)
                                        .center_x()
                                        .center_y(),
                                    )
                                    .on_press(Message::Open(index))
                                    .padding(10),
                                )
                            })
                            .height(230)
                            .spacing(10)
                    )
                    .direction(scrollable::Direction::Horizontal(
                        scrollable::Properties::default()
                    ))
                )
                .max_height(250)
            ]
            .align_items(iced::Alignment::Center)
            .spacing(10)
            .padding(10),
        )
        .center_x()
        .center_y()
        .height(Length::Fill)
        .width(Length::Fill)
        .into()
    }
}
